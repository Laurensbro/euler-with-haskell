import Text.Printf
import Control.Exception
import Data.Time

{--
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

What is the 10001st prime number?

Answer: 104743
--}


isPrime k = null [ x | x <- [2..k - 1], k `mod`x  == 0]

--antw = [x|x<-(2:[3,5..]), isPrime x] !! 10000

--main = print (antw) --geeft antwoord in iets < 1 minuut (gecompileerde versie).
main = do
    start <- getCurrentTime
    printf "Answer: %d \n" ([x|x<-(2:[3,5..]), isPrime x] !! 10000::Int)
    end <- getCurrentTime
    print (diffUTCTime end start)