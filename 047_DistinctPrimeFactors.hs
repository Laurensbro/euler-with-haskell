{--
The first two consecutive numbers to have two distinct prime factors are:

14 = 2 × 7
15 = 3 × 5

The first three consecutive numbers to have three distinct prime factors are:

644 = 2² × 7 × 23
645 = 3 × 5 × 43
646 = 2 × 17 × 19.

Find the first four consecutive integers to have four distinct prime factors. What is the first of these numbers?

--}
import Data.List
divisableBy i x = i `mod` x == 0

--allPrimeFactors:: Integer i => i -> [i]	
allPrimeFactors 1 = []
--allPrimeFactors i = x:allPrimeFactors i/x
--	where x = head [x|x<-[2..i], divisableBy i x]  -- if i mod x (x uit lijst 2..i/2) == 0 -> return [x] ++ allPrimeFactors i/x

--allPrimeFactors i = [x|x<-[2..i], divisableBy i x]

allPrimeFactors i = [j] ++ (allPrimeFactors (i `div` j))
	where j = head [x|x<-[2..i], divisableBy i x];
	
uniquePrimeFactors i = nub (allPrimeFactors i)

antw = head [x|x<-[1..], 
	length (uniquePrimeFactors x) == 4 &&
	length (uniquePrimeFactors (x+1)) == 4 &&
	length (uniquePrimeFactors (x+2)) == 4 &&
 	length (uniquePrimeFactors (x+3)) == 4
	]
	
main = print antw --134043, duurt wel een paar minuten, gecompileerd zelfs.