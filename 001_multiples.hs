import Text.Printf
import Control.Exception
import System.CPUTime
import Data.List
import Data.Time

--multiples of 3 and 5 < 1000. Answer = 233168
triples = [3, 6 .. 999]
quintuples = [5, 10 .. 995]

sumsList [] = 0
sumsList [x] = x
sumsList (x:xs) = x + sumsList xs

main = do
    start <- getCurrentTime
    printf "Answer: %d \n" (sumsList (nub (triples ++ quintuples))::Int)
    end <- getCurrentTime
    print (diffUTCTime end start)
