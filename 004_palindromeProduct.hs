--A palindromic number reads the same both ways. The largest palindrome made from the --product of two 2-digit numbers is 9009 = 91 × 99.
--
--Find the largest palindrome made from the product of two 3-digit numbers.
--Answer: 906609

stringIsPalindrome :: [Char] -> Bool
stringIsPalindrome [] = True
stringIsPalindrome [x] = True
stringIsPalindrome (x:xs) = (x == last xs && stringIsPalindrome (init xs))

allePalindromen = [(x*y, x, y)|x<-[100..999], y<-[100..999], stringIsPalindrome (show (x*y))]
antwoord = maximum allePalindromen --is idd 906609 - 993 x 913
