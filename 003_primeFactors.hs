--The prime factors of 13195 are 5, 7, 13 and 29.
--What is the largest prime factor of the number 600851475143 ?

--Poging 1.. duurt langer dan 1 minuut dus cancel.
--delers = [x|x<-[600851475142, 600851475141..0], mod 600851475143 x == 0]

basisGetal = 600851475143 :: Int
beginGetal = (basisGetal `div` 2) :: Int --dat scheelt al de helft aan zoeken
--aangezien de deler ook niet een even getal is, kan je ook steeds -2 doen. Scheelt al weer de helft.
--delers = [x|x<-[beginGetal,(beginGetal-2)..0], (mod (basisGetal) x == 0)]
--duurt ook te lang (+5min gecanceld)

-- target = deler_1 x deler_2
-- deler_1 is het grote getal, deler_2 het kleine
-- dan zit deler_1 tussen 600Mrd en wortel 600Mrd, 
-- en deler_2 tussen 3 en wortel 600Mrd
-- wortel 600Mrd is < 1 Miljoen, dus de range 600Mrd->1Mln is veel groter dan 3-1Mln.
-- Beter dan andersom beginnen.
sqBasis = (floor . sqrt . fromIntegral) basisGetal ::Int
delers = [x|x<-[3, 5..sqBasis], (mod (basisGetal) x == 0)]

isPrime k = null [ x | x <- [2..k - 1], k `mod`x  == 0]
				
antwoord = last [x|x<-delers, isPrime x]