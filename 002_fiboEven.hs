fibo 1 = 1
fibo 2 = 2
fibo x = fibo (x-1) + fibo (x-2)

antwoord = takeWhile (< 4000000) [fibo x|x<-[1..]]
evenAntwoorden = [x|x<-antwoord,even x]
sumEven = sum evenAntwoorden
--antwoord = [fibo x|x<-[1..], fibo x < 4000000]