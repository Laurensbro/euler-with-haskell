{--
The square root of 2 can be written as an infinite continued fraction.
√2 = 1 + 	1
  	2 + 	1
  	  	2 + 1
  	  	  	2 + 	1
  	  	  	  	2 + ...

The infinite continued fraction can be written, √2 = [1;(2)], (2) indicates that 2 repeats ad infinitum. In a similar way, √23 = [4;(1,3,1,8)].

It turns out that the sequence of partial values of continued fractions for square roots provide the best rational approximations. Let us consider the convergents for √2.

https://projecteuler.net/problem=65 voor uitleg in goeie layout

Hence the sequence of the first ten convergents for √2 are:
1, 3/2, 7/5, 17/12, 41/29, 99/70, 239/169, 577/408, 1393/985, 3363/2378, ...

What is most surprising is that the important mathematical constant,
e = [2; 1,2,1, 1,4,1, 1,6,1 , ... , 1,2k,1, ...].

The first ten terms in the sequence of convergents for e are:
2, 3, 8/3, 11/4, 19/7, 87/32, 106/39, 193/71, 1264/465, 1457/536, ...

The sum of digits in the numerator of the 10th convergent is 1+4+5+7=17.

Find the sum of digits in the numerator of the 100th convergent of the continued fraction for e.

--}
module Main where
 
--netjesInDelen 2 1 2 -- 2 + 1/2 -> (5,2)
netjesInDelen g t n = (g*n+t,n) -- (5,2)


inverse (a,b) = (b,a)
--convergent 100 xs = (1,xs[99])

convergent 100 xs = (1,xs!!98)
convergent 1 xs = (2 * snd nextConvergent + fst nextConvergent, snd nextConvergent)
	where nextConvergent = convergent 2 xs
convergent n xs = inverse (netjesInDelen (xs!!(n-2)) (fst nextConvergent) (snd nextConvergent)) 
	where nextConvergent = convergent (n+1) xs

lijstVanTriples = [[1,x,1]|x<-[2,4..]]
lijstSamengevoegd = [x|y<-lijstVanTriples, x<-y]

uitkomstEBij100 = (convergent 1 lijstSamengevoegd)
teller = show (fst uitkomstEBij100)
inLosseGetallen = [(read [x]::Integer)|x<-teller]
antwoord = sum inLosseGetallen
