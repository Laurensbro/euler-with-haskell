{--
The arithmetic sequence, 1487, 4817, 8147, in which each of the terms increases by 3330, is unusual in two ways: (i) each of the three terms are prime, and, (ii) each of the 4-digit numbers are permutations of one another.

There are no arithmetic sequences made up of three 1-, 2-, or 3-digit primes, exhibiting this property, but there is one other 4-digit increasing sequence.

What 12-digit number do you form by concatenating the three terms in this sequence?

!! Een arithmetic sequence is bv 2,4,6 of 4,8,12,16 etc -> lineair oftewel verschil tussen 2 opvolgers moet gelijk zijn. Hier ging ik eerst de mist in.

--}
import Data.List
isPrime k = null [ x | x <- [2..k - 1], k `mod`x  == 0]

primePermutations a b = [(read x::Integer)|x<-permutations (show a), (read x)/=b, (read x)/=a, isPrime (read x)]

--getallen 1000 - 9999 bekijken. Per priemgetal alle permutaties langs (4x3x2 = 24 stuks)
antw = [(x,y,z)|x<-[1000..9999], isPrime x, y<-primePermutations x x, z<-primePermutations x y, x < y, y < z, z-y == y-x] 

--netjes 296962999629, maar antwoord komt 4x, omdat de permutaties van 2969 2x 9 bevat. 
--Ik zou de dubbelen er uit kunnen halen voor de volledigheid, maar tis wel prima zo. 
-- +/- 1 minuut in de interpreter.
