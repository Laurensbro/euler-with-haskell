module Main where
 
--2520 is the smallest number that can be divided by each of the numbers from 1 to 10 --without any remainder.

--What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
--Answer:232792560

--aanpak: vermenigvuldig alle getallen 1 t/m 20, maar niet als ze al in een vermenigvuldiging van andere getallen zitten. Bv 20 = 10 * 2, dan 10 en 2 niet meenemen.

dividableBy a [] = True
dividableBy a (x:xs) = a `mod` x ==0 && dividableBy a xs

main = print (head [x|x<-[20, 40..], dividableBy x [20, 19..11]])

