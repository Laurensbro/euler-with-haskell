{--
A number chain is created by continuously adding the square of the digits in a number to form a new number until it has been seen before.

For example,

44 → 32 → 13 → 10 → 1 → 1
85 → 89 → 145 → 42 → 20 → 4 → 16 → 37 → 58 → 89

Therefore any chain that arrives at 1 or 89 will become stuck in an endless loop. What is most amazing is that EVERY starting number will eventually arrive at 1 or 89.

How many starting numbers below ten million will arrive at 89?

Aanpak: recursieve functie, stop bij 1 of 89 en geef dan true of false. Count van allemaal waar true < 10.000.000
todo: eens uitzoeken waarom x**2 niet werkt als het uit een andere functie komt.. ([x**2|x<-[1,2]] werkt wel, maar niet uit getLosseGetallen)..
--}


getLosseGetallen xs = [(read [x]::Integer)|x<-xs]

getSumOfSquares ixs = sum [x*x|x<-getLosseGetallen (show ixs)]

getOneOrEightyNine n 
	| n==89 = True
	| n==1 = False	
	| otherwise = getOneOrEightyNine (getSumOfSquares n)
	
main = print (length [x|x<-[1..9999999], getOneOrEightyNine x]) -- 8581146